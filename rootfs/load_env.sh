#!/bin/bash

sed -i "/DBHOST/c\define\(\'DBHOST\', \'${DBHOST}\'\);" /opt/prestige/api/config.php
sed -i "/DBNAME/c\define\(\'DBNAME\', \'${DBNAME}\'\);" /opt/prestige/api/config.php
sed -i "/DBUSER/c\define\(\'DBUSER\', \'${DBUSER}\'\);" /opt/prestige/api/config.php
sed -i "/DBPASSWORD/c\define\(\'DBPASSWORD\', \'${DBPASS}\'\);" /opt/prestige/api/config.php
#sed -i "/EXCLUDED_ROUTES/c\define\(\'EXCLUDED_ROUTES\', \'${EXCLUDED_ROUTES}\'\);" /opt/prestige/api/config.php

## Broken
# if [[ $ENABLE_OAUTH == *"1"* ]]; then sed -i "/ENABLE_OAUTH/c\define\(\'ENABLE_OAUTH\', true\);" /opt/prestige/api/config.php; else sed -i "/ENABLE_OAUTH/c\define\(\'ENABLE_OAUTH\', false\);" /opt/prestige/api/config.php; fi

if [[ $DEFAULT_LOGIN_API == *"1"* ]]; then sed -i "/DEFAULT_LOGIN_API/c\define\(\'DEFAULT_LOGIN_API\', true\);" /opt/prestige/api/config.php; else sed -i "/DEFAULT_LOGIN_API/c\define\(\'DEFAULT_LOGIN_API\', false\);" /opt/prestige/api/config.php; fi

if [[ $ENABLE_OPEN_REGISTRATIONS == *"1"* ]]; then sed -i "/ENABLE_OPEN_REGISTRATIONS/c\define\(\'ENABLE_OPEN_REGISTRATIONS\', true\);" /opt/prestige/api/config.php; else sed -i "/ENABLE_OPEN_REGISTRATIONS/c\define\(\'ENABLE_OPEN_REGISTRATIONS\', false\);" /opt/prestige/api/config.php; fi

if [[ $DEFAULT_SAAS_MODE == *"1"* ]]; then sed -i "/DEFAULT_SAAS_MODE/c\define\(\'DEFAULT_SAAS_MODE\', true\);" /opt/prestige/api/config.php; else sed -i "/DEFAULT_SAAS_MODE/c\define\(\'DEFAULT_SAAS_MODE\', false\);" /opt/prestige/api/config.php; fi

if [[ $LEGACY_MODE == *"1"* ]]; then sed -i "/LEGACY_MODE/c\define\(\'LEGACY_MODE\', true\);" /opt/prestige/api/config.php; else sed -i "/LEGACY_MODE/c\define\(\'LEGACY_MODE\', false\);" /opt/prestige/api/config.php; fi

if [[ ! ($API_SCRIPT_URL == "") ]]; then curl $API_SCRIPT_URL > /opt/prestige/ide/workspace/api/index.php; else echo "No API Script!!!" & cat /rootfs/default_api_script.php > /opt/prestige/ide/workspace/api/index.php; fi

if [[ $PRODUCTION_MODE == *"1"* ]]; then rm -rf /opt/prestige/db /opt/prestige/terminal /opt/prestige/builder /opt/prestige/editor  /opt/prestige/python /opt/prestige/node /opt/prestige/lua /opt/prestige/launch /opt/prestige/ide/index.php /opt/prestige/fm /opt/prestige/api/docs /opt/prestige/api/configure/index.php  /opt/prestige/api/configure/secure.php ; else echo "Running in Development Mode! (Unsafe, use -e PRODUCTION_MODE=1)" ; fi

if [[ ! ($TEST_FILE_URL == "") ]]; then curl $TEST_FILE_URL > /opt/prestige/$TEST_FILE_NAME; fi