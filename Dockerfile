FROM ubuntu:18.04
# TODO: Change to Alpine 

# Update & Install
RUN apt update -y
RUN apt upgrade -y
RUN apt install -y git
RUN DEBIAN_FRONTEND=noninteractive apt install -y php7.2 php7.2-cli php7.2-common php7.2-mbstring php7.2-gd php7.2-intl php7.2-xml php7.2-mysql php7.2-zip php7.2-curl
RUN DEBIAN_FRONTEND=noninteractive apt install -y libapache2-mod-php7.2
RUN printf "13\n33\n" | DEBIAN_FRONTEND=noninteractive apt install -y php-dev libmcrypt-dev php-pear
RUN printf "\n" | pecl channel-update pecl.php.net
RUN printf "\n" | pecl install mcrypt-1.0.1
RUN sed -i '1a\extension=mcrypt.so' /etc/php/7.2/cli/php.ini
RUN sed -i '1a\extension=mcrypt.so' /etc/php/7.2/apache2/php.ini

# Clone Code
RUN git clone https://github.com/geekypedia/prestige /opt/prestige

# Remove Unwanted packages from final Image
#RUN apt purge -y git
#RUN apt autoremaove

# Copy rootfs
COPY ./rootfs /rootfs
RUN chmod +x -R /rootfs/*

# Initialize Configuration
RUN cat /rootfs/prestige/api/config.php > /opt/prestige/api/config.php
#RUN cat /rootfs/prestige.config > /opt/prestige/api/prestige.config
#RUN cat /rootfs/prestige.key > /opt/prestige/api/prestige.key

# Copy Improved Scripts
RUN cat /rootfs/prestige/api/index.php > /opt/prestige/api/index.php
RUN cat /rootfs/prestige/api/versions/1.0.0.php > /opt/prestige/api/versions/1.0.0.php
RUN cat /rootfs/prestige/api/cache.php > /opt/prestige/api/cache.php

# Set Environment
ENV DBHOST=db.example.com
ENV DBNAME=example_db
ENV DBUSER=example_db_user
ENV DBPASS=example_db_pass
ENV ENABLE_OAUTH=0
ENV ENABLE_OPEN_REGISTRATIONS=1
ENV DEFAULT_LOGIN_API=1
ENV DEFAULT_SAAS_MODE=0
ENV EXCLUDED_ROUTES=
ENV LEGACY_MODE=0
ENV PRODUCTION_MODE=0
ENV TEST_FILE_URL=
ENV TEST_FILE_NAME=test.txt
ENV PORT=8080

# Set Expose Port
EXPOSE 8080

# Command
CMD /rootfs/load_env.sh && php -S 0.0.0.0:$PORT -t /opt/prestige
